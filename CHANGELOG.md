# Change Log

## [1.2.9] 
- New Button "Compile current file". And fix bugs with L series.

## [1.2.8] 
- Changed Intellisence settings to "gcc-arm" and "c99" standart.


## [1.2.7] 
- Fixed bugs with custom user paths.


## [1.2.6] 
- Now you can use your custom paths with your own `.c/.h files` and `global includes` in your project (see "Presets" paragraph 7).


## [1.2.5] 
- Add button "OpenCubeMxProject" - opens your CubeMx project in CubeMx app.


## [1.2.4] 
- Don't ask me why.. %D  

## [1.2.3] 
- Correct ".svd" files for STM32F4xx series.
- Add "armToolchainPath" in configurations for "Cortex debug" extention.
- Filled in "Changelog.md" file.


## [1.2.2] 
- Add ".svd" files for STM32F7xx series.


## [1.2.0]
- AutoWrite new ".c" files names into Makefile.
- AutoCorrect Makefile after rebuild your project in CubeMx programm, because CubeMx have a bug with this function.


## [1.0.0]
- First version.