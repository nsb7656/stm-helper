// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';
import * as util from 'util';
import { posix } from 'path';


// для потока риалтайм компиляции текущего файла
import * as inspector from 'inspector';

//
const fs = require('fs');

const _ = require('lodash');
const { window } = require('vscode');

// кнопка для обновления рабочей области
let UpdateWorkspaceBt: vscode.StatusBarItem;
let CompileCurrentFileBt: vscode.StatusBarItem;
let BuildProjectBt: vscode.StatusBarItem;
let DownloadBt: vscode.StatusBarItem;
let OpenCubeMxFileBt: vscode.StatusBarItem;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate({subscriptions}: vscode.ExtensionContext) 
{
	// создаем команду на обновление рабочей области по нажатию кнопки 'UpdateWorkspaceBt'
	const updCommand = 'helper.updateworkspace';
	

	// создаем кнопку для обновления рабочей области
	UpdateWorkspaceBt = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	UpdateWorkspaceBt.text = "UpdateWorkspace";
	UpdateWorkspaceBt.command = updCommand;
	subscriptions.push(vscode.commands.registerCommand(updCommand, UpdateWorkspaceBtClick));
	UpdateWorkspaceBt.show();

	// создаем команду на компилирование текущего открытого файла
	const CompileCurrFileCommand = 'helper.CompileCurrentFile';

	// создаем кнопку для сборки проекта
	BuildProjectBt = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	BuildProjectBt.text = "CompileCurrentFile";
	BuildProjectBt.command = CompileCurrFileCommand;
	subscriptions.push(vscode.commands.registerCommand(CompileCurrFileCommand, CompileCurrentFileBtClick));
	BuildProjectBt.show();


	// создаем команду на сборку проекта по нажатию кнопки 'BuildProject'
	const BuildCommand = 'helper.Build';

	// создаем кнопку для сборки проекта
	BuildProjectBt = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	BuildProjectBt.text = "Build";
	BuildProjectBt.command = BuildCommand;
	subscriptions.push(vscode.commands.registerCommand(BuildCommand, BuildProjectBtClick));
	BuildProjectBt.show();

	// создаем команду на сборку проекта по нажатию кнопки 'BuildProject'
	const DownloadCommand = 'helper.Download';

	// создаем кнопку для сборки проекта
	DownloadBt = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	DownloadBt.text = "Download";
	DownloadBt.command = DownloadCommand;
	subscriptions.push(vscode.commands.registerCommand(DownloadCommand, DownloadBtClick));
	DownloadBt.show();


	// создаем команду на открытие файла проекта CubeMx
	const OpenCubeCommand = 'helper.OpenCubeMxProject';

	// создаем кнопку для открытия проекта CubeMx
	OpenCubeMxFileBt = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 100);
	OpenCubeMxFileBt.text = "OpenCubeMxProject";
	OpenCubeMxFileBt.command = OpenCubeCommand;
	subscriptions.push(vscode.commands.registerCommand(OpenCubeCommand, OpenCubeBtClick));
	OpenCubeMxFileBt.show();




	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Extension "STM-Helper" is now active!');
	//vscode.window.showInformationMessage(`Extension "STM-Helper" is now active!`);
	
}

// this method is called when your extension is deactivated
export function deactivate() {}

/**
 * @description  функция, для компиляции открытого текущего документа
 */
async function CompileCurrentFileBtClick(): Promise<void> 
{	
	const OpendFile = vscode.window.activeTextEditor;

	// проверка на наличие открытого файла (редактора)
	if(OpendFile)
	{
		vscode.commands.executeCommand("workbench.action.tasks.runTask", "Compile current file");
	}
	else
	{
		vscode.window.showInformationMessage(`No open fle!`);
	}
}



/**
 * @description  функция, вызывемая при нажатии кноки "UpdateWorkspaceBt"
 */
async function UpdateWorkspaceBtClick(): Promise<void> 
{	
	// проверим наличие рабочей области
	if(!vscode.workspace.workspaceFolders) 
	{
		vscode.window.showInformationMessage(`No WorkSpace area!`);
		return;
	}


	// получим URI корневой папки (рабочей области) (ws -> workspace)
	let ws: vscode.Uri;
	ws = vscode.workspace.workspaceFolders![0]!.uri;

	// проверим, создаем папку ".vscode" в рабочей области
	vscode.workspace.fs.createDirectory(ws.with({ path: posix.join(ws.path, '.vscode') }));

	// создадим и заполним settings.json файл
	await CreateSettngsJsonFile();
	

	// создадим Launch.json файл
	if(!(await CreateLaunchJsonFile()))
	{
		// если не удалось создать файл или заполнить его, сбрасываем обновление рабочей области
		vscode.window.showInformationMessage(`Error, when we try to create Launch.json file!`);
		return;
	}
	

	if(!(await CreateCCppPropertiesJsonFile()))
	{
		// если не удалось создать файл или заполнить его, сбрасываем обновление рабочей области
		vscode.window.showInformationMessage(`Error, when we try to create c_cpp_properties.json file!`);
		return;
	}

	if(!(await CreateTasksJsonFile()))
	{
		// если не удалось создать файл или заполнить его, сбрасываем обновление рабочей области
		vscode.window.showInformationMessage(`Error, when we try to create tasks.json file!`);
		return;
	}
		
	// Если все удачно - выведем оповещание
	vscode.window.showInformationMessage(`Workspace - successfully updated!`);
}

/**
 * @description  функция, вызывемая при нажатии кноки "Build"
 */
async function BuildProjectBtClick(): Promise<void>
{
	// найдем и переконфигурируем Makefile, если это требуется
	if(!(await CheckAndEditMakeFile())) 
	{
		vscode.window.showInformationMessage(`Error in reconfiguration MakeFile!`);
		return;
	}

	// после обновления Make файла, надо, чтобы в Tasks тоже добавились пользовательские инклюды
	if(!(await CreateTasksJsonFile()))
	{
		vscode.window.showInformationMessage(`Error in reconfiguration TasksFile!`);
		return;
	}
	else
	{
		// запустим команду сборки проекта
		//vscode.commands.executeCommand("workbench.action.tasks.runTask", "Compile current file");
		vscode.commands.executeCommand("workbench.action.tasks.runTask", "Build project");			
	}

}

/**
 * @description  функция, вызывемая при нажатии кноки "Download"
 */
function DownloadBtClick()
{
	vscode.commands.executeCommand("workbench.action.tasks.runTask", "Download");
}

/**
 * функция, вызывемая при нажатии кноки "OpenCubeMxProject"
 */
async function OpenCubeBtClick(): Promise<void> 
{	
	// проверим наличие рабочей области
	if(!vscode.workspace.workspaceFolders) 
	{
		vscode.window.showInformationMessage(`No WorkSpace area!`);
		return;
	}

	// пробуем открыть файл проекта
	if(!(await OpenCubeMxApp()))
	{
		vscode.window.showInformationMessage(`Can't open CubeMx project!`);
		return;
	}
}


/**
 * @description  функция для проверки и автоматического редактирования Make файла
 * @returns {boolean} true - если файл успешно создан
 */
async function CheckAndEditMakeFile(): Promise<boolean>
{
	// новый Make файл
	let NewMakeString: string;
	let BufferMaketring: string;						// промежуточный буффер для make файла

	// проверяем наличие рабочей области 
	if(!vscode.workspace.name)
	{
		return false;
	}

	// 1. получим содержимое Makefile
	let Makefile: string = await GetMakeFileStringData();
	if(Makefile === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	// 2. выделим в новый Makefile первую часть Make файла до C_SOURCES
	var ArrayOfMake = Makefile.split("C_SOURCES =  \\");
	NewMakeString = ArrayOfMake[0] + "C_SOURCES =  \\" + "\n";

	// 3. выделяем из make файла все строчки с путями до .c файлов
	let CRegex =  new RegExp('.+\\w\\.c', 'g');
	let CArr;
	let CStringArr:string[] = [];
	while(CArr = CRegex.exec(Makefile))
	{
		CStringArr.push(CArr![0]);			// добавляем новый путь к c файлу
	}

	// 4. проверим наличие файлов по всем заисанным путям, если файла не существует - удаляем из списка
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;
	for(var i = 0; i < CStringArr.length; i ++)
	{
		// пробуем прочитать файл
		try 
		{
			await vscode.workspace.fs.readFile( ws.with({ path: posix.join(ws.path, CStringArr[i]) }));
		} 
		catch (error) 
		{
			// удаляем эту строчку из массива
			CStringArr.splice(i,1);
		}
	}

	// 5. получим расположение main.c файла и проверим, подключены ли все файлы из этой папки в make файле
	let MianCRegex =  new RegExp('.*main\\.c', 'g');
	let MainCArr = MianCRegex.exec(Makefile);
	if(!MainCArr)
	{
		// если main файл отсутствует
		return false;
	}
	else
	{
		// 1. получим путь к папке с main.c файлом
		let MainDirectoryString: string =  MainCArr![0];
		MainDirectoryString = MainDirectoryString.split("/main")[0];
		
		// создадим регулярное выражение на поиск src файлов (.с)
		let SrcFileRegex =  new RegExp('.+\\.c$');  	// ищет как .c,
		
		// 2. переберем все файлы записанные по пути располажения main.c файла
		for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, MainDirectoryString) }))) 	// проходимся по всем файлам проекта
		{
			// проверяем, что это файл, а не папка
			if (type === vscode.FileType.File) 
			{
				// проверяем тип файла
				if(SrcFileRegex.exec(name) !== null)
				{
					// создадим флаг
					var ConsistFlag: boolean = false;

					// проверяем, записан ли данный файл в список "С" файлов в MAkefile
					for(var i = 0; i < CStringArr.length; i ++)
					{
						if((MainDirectoryString + "/" + name) === CStringArr[i])
						{
							// если нашли этот файл в списке, поднимаем флаг
							ConsistFlag = true;	
							break;
						}					
					} 

					// проверяем, поднялся флаг или нет
					if(ConsistFlag === false)
					{
						// добавил в список новый элемент
						CStringArr.push(MainDirectoryString + "/" + name);
					}
				}				
			}
		}


		// 3. получим пользовательские пути к c. файлам
		let UserSrcPathRegex =  new RegExp('\\b.+\\b', 'g');			// значение регулярки - "строка целиком"
		let FoundedUserSrcPath: boolean = false;						// флаг того, что нашли пользовательские пути
		let readData;
		let UserSrcPathArr;
		let UserSrcPathStringArr:string[] = [];							// массив путей к пользовательским .c файлам
		let FileAlreadyExist: boolean = false;
		
		// проверим наличие файла c_cpp_properties
		for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, "/.vscode") }))) 	// проходимся по всем файлам проекта
		{
			if(name === "c_cpp_properties.json")
			{
				FileAlreadyExist = true;			// значит файл существует
			}
		}

		// если файл существует
		if(FileAlreadyExist)
		{
			// считываем весь файл c_cpp_properties.json
			if (readData = await vscode.workspace.fs.readFile!(ws.with({ path: posix.join(ws.path, "/.vscode/c_cpp_properties.json") }))) 	
			{
				// считываем построчно все строки из файла - ищем все пользовательские папки с .с файлами
				while(UserSrcPathArr = UserSrcPathRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
				{
					// если уже был найден хоть один пользовательский папки с .с файлами
					if(FoundedUserSrcPath === true)
					{
						if(UserSrcPathArr![0] === "_USER_SRC_PATHS_END_")
						{
							// останавливаем запись папки с .с файлами пользователя
							FoundedUserSrcPath = false;
							break;												// выходим из цикла while
						}
						else
						{
							UserSrcPathStringArr.push(UserSrcPathArr![0]);		// добавляем путь
						}							
					}
					
					// если еще не был найдет ни один define  
					else
					{
						if(UserSrcPathArr![0] === "userSrcPaths")					// если нашли начало пользовательских путей
						{
							FoundedUserSrcPath = true;							// поднимаем флаг					
						}
					}
				}
			}
		}
		
		
		// получили путик к пользовательским .с файлам - теперь добавляем все src файлы из этих путей в промежуточный массив
		for(var i = 0; i < UserSrcPathStringArr.length; i++)
		{
			// проходимся по всем файлам пользовательского пути
			for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, UserSrcPathStringArr[i]) }))) 	
			{
				// проверяем, что это файл, а не папка
				if (type === vscode.FileType.File) 
				{
					// проверяем тип файла
					if(SrcFileRegex.exec(name) !== null)
					{
						// создадим флаг
						var ConsistFlag: boolean = false;
							

						// проверяем, записан ли данный файл в список "С" файлов в MAkefile
						for(var j = 0; j < CStringArr.length; j ++)
						{							
							if((UserSrcPathStringArr[i] + "/" + name) === CStringArr[j])
							{
								// если нашли этот файл в списке, поднимаем флаг
								ConsistFlag = true;	
								break;
							}					
						} 

						// проверяем, поднялся флаг или нет
						if(ConsistFlag === false)
						{
							// добавил в список новый элемент
							CStringArr.push(UserSrcPathStringArr[i] + "/" + name);
						}
					}					
				}
			}
		}
		

	}

	// 6. Запишем в новый Make файл список наших "".c" файлов
	for(var i = 0; i < CStringArr.length; i ++)
	{
		NewMakeString += (CStringArr[i]);
		if(i !== (CStringArr.length - 1))
		{
			NewMakeString += " \\";				// "\" добавляем во все файлы кроме последнего				
		}
		NewMakeString += "\n";	 
	}

	
	
	// 7. Запишем в новый Make файл информацию из страого Make файла до C_INCLUDES
	
	// выделяем кусок сразу поcле .c файлов
	ArrayOfMake = Makefile.split("# ASM sources");					
	BufferMaketring = "\n" + "\n" + "# ASM sources" + ArrayOfMake[1];
	
	// и до начала include файлов
	ArrayOfMake = BufferMaketring.split("C_INCLUDES =  \\");		
	BufferMaketring = ArrayOfMake[0] + "C_INCLUDES =  \\" + "\n";

	// записываем в новый make файл
	NewMakeString += BufferMaketring;


	
	// 8. считаем все include строчки из Make файла:
	let IncArr;
	let IncStringArr:string[] = [];
	let IncString: string;
	let IncRegex =  new RegExp('(?<=\-I).+\\b', 'g'); 
	let ExistFlag:boolean = false;

	// найдем все значения include
	while(IncArr = IncRegex.exec(Makefile))
	{
		// сбрсываем флаг
		ExistFlag = false;

		// ищем совпадения в названиях
		for(var i = 0; i < IncStringArr.length; i++)
		{
			if(IncArr![0] === IncStringArr[i])
			{
				ExistFlag = true;				// поднимаем флаг, что уже существует такой инклюд
				break;
			}
		}

		// проверяем
		if(!ExistFlag)
		{
			IncStringArr.push("-I");			// в начале добавляем -I
			IncStringArr.push(IncArr![0]);		// добавляем include
			IncStringArr.push(" \\\n");			// в конце каждой строчки iclude добавляем 
		}
		
	}
	
	
	// 9. Считываем все пользовательские пути к .h файлам
	let UserPathRegex =  new RegExp('\\b.+\\b', 'g');			// значение регулярки - "строка целиком"
	let FoundedUserPath: boolean = false;						// флаг того, что нашли пользовательские пути
	let readData;
	let UserPathArr;
	let FileAlreadyExist:boolean = false;

	
	// проверим наличие файла c_cpp_properties
	for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, "/.vscode") }))) 	// проходимся по всем файлам проекта
	{
		if(name === "c_cpp_properties.json")
		{
			FileAlreadyExist = true;			// значит файл существует
		}
	}


	// Если уже существует c_cpp_properties.json файл
	if(FileAlreadyExist)
	{
		// считываем весь файл
		if (readData = await vscode.workspace.fs.readFile!(ws.with({ path: posix.join(ws.path, "/.vscode/c_cpp_properties.json") }))) 	
		{
			// считываем построчно все строки из файла - ище все пользовательские define
			while(UserPathArr = UserPathRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
			{
				// если уже был найден хоть один пользовательский .h
				if(FoundedUserPath === true)
				{
					if(UserPathArr![0] === "_USER_INC_PATHS_END_")
					{
						// останавливаем запись define пользователя
						FoundedUserPath = false;
						break;											// выходим из цикла while
					}
					else
					{
						// сбрсываем флаг
						ExistFlag = false;

						// ищем совпадения в названиях
						for(var i = 0; i < IncStringArr.length; i++)
						{
							if(UserPathArr![0] === IncStringArr[i])
							{
								ExistFlag = true;				// поднимаем флаг, что уже существует такой инклюд
								break;
							}
						}

						// проверяем
						if(!ExistFlag)
						{
							IncStringArr.push("-I");			// в начале добавляем -I
							IncStringArr.push(UserPathArr![0]);		// добавляем include
							IncStringArr.push(" \\\n");			// в конце каждой строчки iclude добавляем 
						}

					
					}						
				}
				
				// если еще не был найдет ни один define  
				else
				{
					if(UserPathArr![0] === "userIncPaths")					// если нашли начало пользовательских путей
					{
						FoundedUserPath = true;							// поднимаем флаг					
					}
				}
			}
		}
	}


	//----------------------------------------------------------------------
	IncStringArr.pop();							// уберем последний "\" из списка инклюдов		
	//----------------------------------------------------------------------


	// преобразуем массив  Include в строку
	IncString = IncStringArr.join("");	

	// если строка оказалась пустой - вернем ошибку
	if(IncString === null)
	{
		return false;
	}


	// 10. запишем правильный список include файлов
	NewMakeString += IncString + "\n";

	// 11. Выделяем оставшуюся часть старого Make файла и дозаписываем ее в новый Make файл
	ArrayOfMake = Makefile.split("# compile gcc flags");					
	NewMakeString += ("\n" + "\n" + "# compile gcc flags"  + ArrayOfMake[1]);

	// 12. ОБновляем Make файл, если это требуется
	if(NewMakeString !== Makefile)
	{
		vscode.workspace.fs.writeFile(ws.with({ path: posix.join(ws.path, 'Makefile') }), Buffer.from(NewMakeString));
	}


	return true;
}


/**
 * @description  функция для созадания tasks.json файла
 * @returns {boolean} true - если файл успешно создан
 */
async function CreateTasksJsonFile(): Promise<boolean>
{
	// проверяем наличие рабочей области 
	if(!vscode.workspace.name)
	{
		return false;
	}

	// получим путь к папке
	let ext: string = GetMyExtentionUri().path.slice(1);

	// путь до make файла
	let MakePath: string = ext + "/BuildTools/bin/make.exe";
	
	// путь до armgcc/bin
	let ArmGccBinPath: string = ext + "/ArmGcc/bin";

	// путь к файлу комилятора - arm-none-eabi-gcc.exe
	let ArmGccCompilePath: string = ext + "/ArmGcc/bin/arm-none-eabi-gcc.exe";

	// путь к openocd
	let OpenocdPath: string = ext + "/OpenOCD/bin/openocd.exe";

	// путь к stlink.cfg
	let StlinkCfgAddr: string = ext + "/OpenOCD/scripts/interface/stlink.cfg";

	// получим содержимое Makefile
	let Makefile: string = await GetMakeFileStringData();
	if(Makefile === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	// получим путь к cfg файлу микрконтроллера
	let cfgFilePath: string = await GetMcuCfgPath(Makefile);
	if(cfgFilePath === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}



	// получим список define, include и  из make файла
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;
	// для define:
	let DefArr;
	let DefStringArr:string[] = [];
	let DefString: string;
	// для include:
	let IncArr;
	let IncStringArr:string[] = [];
	let IncString: string;
	// mcpu
	let McpuArr;
	let McpuString: string;
	// mcu
	let McuArr;
	let McuString: string;


	// 1. подготовим форму регулярного выражения для поиска define в  Make файле
	let DefRegex =  new RegExp('(?<=\-D).+\\b', 'g');
		
	// создадим переменную для отслеживания первого чтения define
	let FirstRead: boolean = true;
	// найдем все значения define
	while(DefArr = DefRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			DefStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		DefStringArr.push("\t\t\t\t");			// добавляем табуляцию
		DefStringArr.push("\"-D");				// добавляем символ кавычек
		DefStringArr.push(DefArr![0]);			// добавляем define
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	DefStringArr.push("\",\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	DefString = DefStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(DefString === null)
	{
		return false;
	}





	// 2. подготовим форму регулярного выражения для поиска include в  Make файле
	let IncRegex =  new RegExp('(?<=\-I).+\\b', 'g');
	
	// обновим значение переменной для первого чтения 
	FirstRead = true;
	// найдем все значения include
	while(IncArr = IncRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			IncStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		IncStringArr.push("\t\t\t\t");			// добавляем табуляцию
		IncStringArr.push("\"-I");				// добавляем символ кавычек
		IncStringArr.push(IncArr![0]);			// добавляем include
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	IncStringArr.push("\",\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	IncString = IncStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(IncString === null)
	{
		return false;
	}


	
	// 3. подготовим форму регулярного выражения для поиска mcpu в  Make файле
	let McpuRegex =  new RegExp('(?<=\CPU\ \=).+', 'g');
	McpuArr = McpuRegex.exec(Makefile);

	// преобразуем массив Define в строку
	McpuString = McpuArr!.join("").slice(1);			

	// если строка оказалась пустой - вернем ошибку
	if(McpuString === null)
	{
		return false;
	}



	// 4. подготовим форму регулярного выражения для поиска mcu в  Make файле
	McuString = "\"-mthumb\",";



	
	// контент файла tasks.json
	let Content: string = 
	"{\n" + 
	"\t" + "\"version\": \"2.0.0\"," + "\n" +
	"\t" + "\"tasks\":" + "\n" +
	"\t" + "[" + "\n" +

	// 1. Формируем "Build project"
	"\t\t" + "{" + "\n" +
	"\t\t\t" + "\"label\": \"Build project\","  + "\n" +
	
	"\t\t\t" + "\"group\":" + "\n" +
	"\t\t\t" + "{" + "\n" +
	"\t\t\t\t" + "\"kind\": \"build\"," + "\n" +
	"\t\t\t\t" + "\"isDefault\": true" + "\n" +
	"\t\t\t" + "}," + "\n" +

	"\t\t\t" + "\"type\": \"shell\"," + "\n" +
	"\t\t\t" + "\"command\": \"" + MakePath + "\"," + "\n" +

	"\t\t\t" + "\"args\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"GCC_PATH=" + ArmGccBinPath + "\"," + "\n" +
	"\t\t\t\t" + "\"-j6\"" + "\n" +
	"\t\t\t" + "]," + "\n" +
	
	"\t\t\t" + "\"problemMatcher\":" + "\n" +
	"\t\t\t" + "{" + "\n" +
	"\t\t\t\t" + "\"pattern\":" + "\n" +
	"\t\t\t\t" + "{" + "\n" +
	"\t\t\t\t\t" + "\"regexp\": \"^(.*):(\\\\d+):(\\\\d+):\\\\s+(warning|error):\\\\s+(.*)$\"," + "\n" +
	"\t\t\t\t\t" + "\"file\": 1," + "\n" +
	"\t\t\t\t\t" + "\"line\": 2," + "\n" +
	"\t\t\t\t\t" + "\"column\": 3," + "\n" +
	"\t\t\t\t\t" + "\"severity\": 4," + "\n" +
	"\t\t\t\t\t" + "\"message\": 5" + "\n" +
	"\t\t\t\t" + "}" + "\n" +
	"\t\t\t" + "}," + "\n" +
	
	"\t\t\t" + "\"presentation\":" + "\n" +
	"\t\t\t" + "{" + "\n" +
	"\t\t\t\t" + "\"focus\": true" + "\n" +
	"\t\t\t" + "}" + "\n" +

	"\t\t" + "}," + "\n" +


	// 2. Формируем "Compile current file"
	"\t\t" + "{" + "\n" +
	"\t\t\t" + "\"label\": \"Compile current file\","  + "\n" +
	"\t\t\t" + "\"type\": \"shell\"," + "\n" +
	"\t\t\t" + "\"command\": \"" + ArmGccCompilePath + "\"," + "\n" +
	
	"\t\t\t" + "\"args\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"-c\"," + "\n" +
			   	 DefString! + "\n" +
			   	 IncString! + "\n" +
	"\t\t\t\t" + "\"" + McpuString! + "\"," + "\n" +
	"\t\t\t\t" + McuString! + "\n" +
	"\t\t\t\t" + "\"-Og\"," + "\n" +
	"\t\t\t\t" + "\"-Wall\"," + "\n" +
	"\t\t\t\t" + "\"-fdata-sections\"," + "\n" +
	"\t\t\t\t" + "\"-ffunction-sections\"," + "\n" +
	"\t\t\t\t" + "\"-g\"," + "\n" +
	"\t\t\t\t" + "\"-gdwarf-2\"," + "\n" +
	"\t\t\t\t" + "\"-MMD\"," + "\n" +
	"\t\t\t\t" + "\"-MP\"," + "\n" +
	"\t\t\t\t" + "\"-MF'build/${fileBasenameNoExtension}.d'\"," + "\n" +
	"\t\t\t\t" + "\"'${relativeFile}'\"," + "\n" +
	"\t\t\t\t" + "\"-o\"," + "\n" +
	"\t\t\t\t" + "\"'build/${fileBasenameNoExtension}.o'\"," + "\n" +
	"\t\t\t" + "]," + "\n" +

	"\t\t\t" + "\"problemMatcher\":" + "\n" +
	"\t\t\t" + "{" + "\n" +
	"\t\t\t\t" + "\"pattern\":" + "\n" +
	"\t\t\t\t" + "{" + "\n" +
	"\t\t\t\t\t" + "\"regexp\": \"^(.*):(\\\\d+):(\\\\d+):\\\\s+(warning|error):\\\\s+(.*)$\"," + "\n" +
	"\t\t\t\t\t" + "\"file\": 1," + "\n" +
	"\t\t\t\t\t" + "\"line\": 2," + "\n" +
	"\t\t\t\t\t" + "\"column\": 3," + "\n" +
	"\t\t\t\t\t" + "\"severity\": 4," + "\n" +
	"\t\t\t\t\t" + "\"message\": 5" + "\n" +
	"\t\t\t\t" + "}" + "\n" +
	"\t\t\t" + "}," + "\n" +

	"\t\t\t" + "\"presentation\":" + "\n" +
	"\t\t\t" + "{" + "\n" +
	"\t\t\t\t" + "\"focus\": true" + "\n" +
	"\t\t\t" + "}" + "\n" +

	"\t\t" + "}," + "\n" +

	// 3. Формируем "Download"
	"\t\t" + "{" + "\n" +
	"\t\t\t" + "\"label\": \"Download\","  + "\n" +
	"\t\t\t" + "\"type\": \"shell\"," + "\n" +
	"\t\t\t" + "\"command\": \"" + OpenocdPath + "\"," + "\n" +

	"\t\t\t" + "\"args\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"-f\"," + "\n" +
	"\t\t\t\t\""+ StlinkCfgAddr + "\"" + "," + "\n" +
	"\t\t\t\t" + "\"-f\"," + "\n" +
	"\t\t\t\t\"" + cfgFilePath + "\"," + "\n" +
	"\t\t\t\t" + "\"-c\"," + "\n" +
	"\t\t\t\t" + "\"program build/" + vscode.workspace.name! + ".elf\"," + "\n" +
	"\t\t\t\t" + "\"-c init\"," + "\n" +
	"\t\t\t\t" + "\"-c reset\"," + "\n" +
	"\t\t\t\t" + "\"-c exit\"" + "\n" +
	"\t\t\t" + "]," + "\n" +
	"\t\t\t" + "\"problemMatcher\": []" + "\n" +
	"\t\t" + "}," + "\n" +


	// окончание файла
	"\t" + "]" + "\n" +
	"}\n"; 


	// создаем c_cpp_properties.json файл и заполняем
	vscode.workspace.fs.writeFile(ws.with({ path: posix.join(ws.path, '.vscode\\tasks.json') }), Buffer.from(Content));


	return true;
}



/**
 * @description  функция для созадания settings.json файла
 */
async function CreateSettngsJsonFile():  Promise<void> 
{
	// получим URI корневой папки (рабочей области) (ws -> workspace)
	let vscode_path: vscode.Uri;
	let ws: vscode.Uri;
	ws = vscode.workspace.workspaceFolders![0]!.uri;
	vscode_path = ws.with({ path: posix.join(ws.path, '.vscode') });

	// Uri рабочей области
	let ext: vscode.Uri = GetMyExtentionUri();
	
	// заменим \ на \\ 
	let extPath : string = ext.fsPath + "\\OpenOCD\\bin\\openocd";  
	extPath = extPath.split('\\').join('\\\\');
	
	// создаем контент для записи
	let Content: string = 
	"{\n" + 
	"\t\"cortex-debug.armToolchainPath\": \"\"," + "\n" +
	"\t\"cortex-debug.openocdPath\": " + "\"" + extPath  + "\"" + "\n" + 
	"}";
	
	// создаем файл и заполняем
	vscode.workspace.fs.writeFile(vscode_path.with({ path: posix.join(vscode_path.path, 'settings.json') }), Buffer.from(Content));

}



/**
 * @description  функция для созадания c_cpp_properties.json файла
 * @returns {boolean} true - если файл успешно создан
 */
async function CreateCCppPropertiesJsonFile_old(): Promise<boolean> 
{
	// проверяем наличие рабочей области 
	if(!vscode.workspace.name)
	{
		return false;
	}

	// проверим, создан ли уже c_cpp_properties файл, если да, то сохраним пользовательские настройки
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;
	let readData;
	let UserDefArr;
	let UserDefStringArr:string[] = [];
	let UserDefString: string = "";
	for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, "/.vscode") }))) 	// проходимся по всем файлам проекта
	{
		if(name === "c_cpp_properties.json")
		{
			if (readData = await vscode.workspace.fs.readFile!(ws.with({ path: posix.join(ws.path, "/.vscode/c_cpp_properties.json") }))) 	// проходимся по всем файлам проекта
			{
				// 1. подготовим форму регулярного выражения для поиска пользовательских параметров
				let UserDefRegex =  new RegExp('\\b.+\\b', 'g');
				let FoundedUserDef: boolean = false;						// флаг того, что нашли поля с пользовательскими define

				// создадим переменную для отслеживания первого чтения define
				let UserFirstRead: boolean = true;
				// пройдемся по всем строкам файла
				while(UserDefArr = UserDefRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
				{
					// если получаем define, которые ввел пользователь
					if(FoundedUserDef === true)
					{
						if(UserDefArr![0] === "_USER_DEF_END_")
						{
							// останавливаем запись define пользователя
							FoundedUserDef = false;
							break;							// выходим из цикла while
						}
						else
						{
							if(!UserFirstRead)								// если это не первая запись, то надо перед следующим элементом добавить
							{
								UserDefStringArr.push("\", \n");			// символ кавычек, запятую и символ новой строки				
							}
								UserDefStringArr.push("\t\t\t");			// добавляем табуляцию
								UserDefStringArr.push("\"");				// добавляем символ кавычек
								UserDefStringArr.push(UserDefArr![0]);		// добавляем define
								UserFirstRead = false;						// сбрасываем флаг первого чтения
							}							
					}
					else
					{
						if(UserDefArr![0] === "userDefines")
						{
							FoundedUserDef = true;							
						}
					}
				}
				
				// проверим, есть ли хоть один пользовательский define
				if(UserDefStringArr.length !== 0)
				{
					// добавляем символ кавычек и новой строки
					UserDefStringArr.push("\"\n");				
					// преобразуем массив пользовательсих Define в строку
					UserDefString = UserDefStringArr.join("");	
				}

				


			}
		}
	}


	// получим значение для поля name, которое является названием проекта:
	let NameField: string = vscode.workspace.name!;

	// получим путь к файлу комилятора - arm-none-eabi-gcc.exe
	let ext: vscode.Uri = GetMyExtentionUri();
	let ArmGccCompilePath: string = ext.path.slice(1) + "/ArmGcc/bin/arm-none-eabi-gcc.exe";

	// получим путь к include файлам комилятора - arm-none-eabi-gcc.exe
	let ArmGccIncPath: string = ext.path.slice(1) + "/ArmGcc/lib/gcc/arm-none-eabi/9.2.1/include";

	// получим содержимое Makefile
	let Makefile: string = await GetMakeFileStringData();
	if(Makefile === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	// получим список define, include из make файла
	// для define:
	let DefArr;
	let DefStringArr:string[] = [];
	let DefString: string;
	// для include:
	let IncArr;
	let IncStringArr:string[] = [];
	let IncString: string;


	// 1. подготовим форму регулярного выражения для поиска define в  Make файле
	let DefRegex =  new RegExp('(?<=\-D).+\\b', 'g');
		
	// создадим переменную для отслеживания первого чтения define
	let FirstRead: boolean = true;
	// найдем все значения define
	while(DefArr = DefRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			DefStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		DefStringArr.push("\t\t\t\t");			// добавляем табуляцию
		DefStringArr.push("\"");				// добавляем символ кавычек
		DefStringArr.push(DefArr![0]);			// добавляем define
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	DefStringArr.push("\"\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	DefString = DefStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(DefString === null)
	{
		return false;
	}





	// 2. подготовим форму регулярного выражения для поиска include в  Make файле
	let IncRegex =  new RegExp('(?<=\-I).+\\b', 'g');
	
	// обновим значение переменной для первого чтения 
	FirstRead = true;
	// найдем все значения include
	while(IncArr = IncRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			IncStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		IncStringArr.push("\t\t\t\t");			// добавляем табуляцию
		IncStringArr.push("\"");				// добавляем символ кавычек
		IncStringArr.push(IncArr![0]);			// добавляем include
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	IncStringArr.push("\"\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	IncString = IncStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(IncString === null)
	{
		return false;
	}

	

	// получим URI корневой папки (рабочей области) (ws -> workspace)
	let vscode_path: vscode.Uri;
	vscode_path = ws.with({ path: posix.join(ws.path, '.vscode') });


	// создаем контент для записи
	let Content: string = 
	"{\n" + 
	"\t\"version\": 4," + "\n" +
	"\t\"env\":" + "\n" +
	"\t{" + "\n" +
	"\t\t" + "\"_USER_DEF_START_\": \"\"," + "\n" +
	"\t\t" + "\"userDefines\": " + "\n" +
	"\t\t" + "[" + "\n" +
			UserDefString! +
	"\t\t" + "]," + "\n" +
	"\t\t" + "\"_USER_DEF_END_\": \"\"" + "\n" +
	"\t}," + "\n" +
	"\t\"configurations\":" + "\n" +
	"\t[" + "\n" +
	"\t\t{" + "\n" +
	"\t\t\t" + "\"name\": \"" + NameField + "\"," + "\n" +
	"\t\t\t" + "\"intelliSenseMode\": \"gcc-x64\"," + "\n" +
	"\t\t\t" + "\"cStandard\": \"c11\"," + "\n" +
	"\t\t\t" + "\"cppStandard\": \"c++17\"," + "\n" +
	"\t\t\t" + "\"compilerPath\": \"" + ArmGccCompilePath + "\"," + "\n" +
	"\t\t\t" + "\"defines\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"${userDefines}\"," + "\n" +
				DefString! + 
	"\t\t\t" + "]," + "\n" +
	"\t\t\t" + "\"includePath\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"${workspaceFolder}\"," + "\n" +
	"\t\t\t\t" + "\"" + ArmGccIncPath + "\"," + "\n" +
				IncString! + 
	"\t\t\t" + "]" + "\n" +
	"\t\t}" + "\n" +
	"\t]" + "\n" +
	"}";

	// создаем c_cpp_properties.json файл и заполняем
	vscode.workspace.fs.writeFile(vscode_path.with({ path: posix.join(vscode_path.path, 'c_cpp_properties.json') }), Buffer.from(Content));


	return true;
}

/**
 * @description  функция для созадания c_cpp_properties.json файла - с поиском всех файлов по всем папкам
 * @returns {boolean} true - если файл успешно создан
 */
async function CreateCCppPropertiesJsonFile(): Promise<boolean> 
{
	// проверяем наличие рабочей области 
	if(!vscode.workspace.name)
	{
		return false;
	}

	// проверим, создан ли уже c_cpp_properties файл, если да, то сохраним пользовательские настройки
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;
	let readData;
	let UserDefArr;
	let UserPathArr;
	let UserSrcPathArr;
	let UserDefStringArr:string[] = [];
	let UserPathStringArr:string[] = [];
	let UserSrcPathStringArr:string[] = [];
	let UserDefString: string = "";
	let UserPathString: string = "";
	let UserSrcString: string = "";
	let FileAlreadyExist: boolean = false;


	// проверим наличие файла c_cpp_properties
	for (const [name, type] of await vscode.workspace.fs.readDirectory(ws.with({ path: posix.join(ws.path, "/.vscode") }))) 	// проходимся по всем файлам проекта
	{
		if(name === "c_cpp_properties.json")
		{
			FileAlreadyExist = true;			// значит файл существует
		}
	}


	// Если уже существует c_cpp_properties.json файл
	if(FileAlreadyExist)
	{
		// считываем весь файл
		if (readData = await vscode.workspace.fs.readFile!(ws.with({ path: posix.join(ws.path, "/.vscode/c_cpp_properties.json") }))) 	
		{
			// 1. подготовим форму регулярного выражения для поиска пользовательских параметров
			let UserDefRegex =  new RegExp('\\b.+\\b', 'g');			// значение регулярки - "строка целиком"
			let FoundedUserDef: boolean = false;						// флаг того, что нашли поля с пользовательскими define

			// создадим переменную для отслеживания первого чтения define
			let UserFirstRead: boolean = true;
			
			// считываем построчно все строки из файла - ище все пользовательские define
			while(UserDefArr = UserDefRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
			{
				// если уже был найден хоть один пользовательский define
				if(FoundedUserDef === true)
				{
					if(UserDefArr![0] === "_USER_DEF_END_")
					{
						// останавливаем запись define пользователя
						FoundedUserDef = false;
						break;											// выходим из цикла while
					}
					else
					{
						if(!UserFirstRead)								// если это не первая запись, то надо перед следующим элементом добавить
						{
							UserDefStringArr.push("\", \n");			// символ кавычек, запятую и символ новой строки				
						}
							UserDefStringArr.push("\t\t\t");			// добавляем табуляцию
							UserDefStringArr.push("\"");				// добавляем символ кавычек
							UserDefStringArr.push(UserDefArr![0]);		// добавляем define
							UserFirstRead = false;						// сбрасываем флаг первого чтения
						}							
				}
				
				// если еще не был найдет ни один define  
				else
				{
					if(UserDefArr![0] === "userDefines")				// если нашли начало пользовательских define
					{
						FoundedUserDef = true;							// поднимаем флаг					
					}
				}
			}


			// проверим, есть ли хоть один пользовательский define
			if(UserDefStringArr.length !== 0)
			{
				// добавляем символ кавычек и новой строки
				UserDefStringArr.push("\"\n");				
				// преобразуем массив пользовательсих Define в строку
				UserDefString = UserDefStringArr.join("");	
			}



			// 2. подготовим форму регулярного выражения для поиска пользовательских путей к .h файлам
			let UserPathRegex =  new RegExp('\\b.+\\b', 'g');			// значение регулярки - "строка целиком"
			let FoundedUserPath: boolean = false;						// флаг того, что нашли пользовательские пути

			// Обновим переменную для отслеживания первого чтения пользовательских путей к папкам
			UserFirstRead = true;
			
			// считываем построчно все строки из файла - ище все пользовательские define
			while(UserPathArr = UserPathRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
			{
				// если уже был найден хоть один пользовательский define
				if(FoundedUserPath === true)
				{
					if(UserPathArr![0] === "_USER_INC_PATHS_END_")
					{
						// останавливаем запись define пользователя
						FoundedUserPath = false;
						break;											// выходим из цикла while
					}
					else
					{
						if(!UserFirstRead)								// если это не первая запись, то надо перед следующим элементом добавить
						{
							UserPathStringArr.push("\", \n");			// символ кавычек, запятую и символ новой строки				
						}
							UserPathStringArr.push("\t\t\t");			// добавляем табуляцию
							UserPathStringArr.push("\"");				// добавляем символ кавычек
							UserPathStringArr.push(UserPathArr![0]);	// добавляем путь
							UserFirstRead = false;						// сбрасываем флаг первого чтения
						}							
				}
				
				// если еще не был найдет ни один define  
				else
				{
					if(UserPathArr![0] === "userIncPaths")					// если нашли начало пользовательских путей
					{
						FoundedUserPath = true;							// поднимаем флаг					
					}
				}
			}
			
			// проверим, есть ли хоть один пользовательских путей
			if(UserPathStringArr.length !== 0)
			{
				// добавляем символ кавычек и новой строки
				UserPathStringArr.push("\"\n");				
				// преобразуем массив пользовательсих Define в строку
				UserPathString = UserPathStringArr.join("");	
			}


			// 3. подготовим форму регулярного выражения для поиска пользовательских путей к .c файлам
			let UserSrcPathRegex =  new RegExp('\\b.+\\b', 'g');			// значение регулярки - "строка целиком"
			let FoundedUserSrcPath: boolean = false;						// флаг того, что нашли пользовательские пути

			// Обновим переменную для отслеживания первого чтения пользовательских путей к папкам
			UserFirstRead = true;
			
			// считываем построчно все строки из файла - ище все пользовательские define
			while(UserSrcPathArr = UserSrcPathRegex.exec(new util.TextDecoder("utf-8").decode(readData)))
			{
				// если уже был найден хоть один пользовательский define
				if(FoundedUserSrcPath === true)
				{
					if(UserSrcPathArr![0] === "_USER_SRC_PATHS_END_")
					{
						// останавливаем запись define пользователя
						FoundedUserSrcPath = false;
						break;												// выходим из цикла while
					}
					else
					{
						if(!UserFirstRead)									// если это не первая запись, то надо перед следующим элементом добавить
						{
							UserSrcPathStringArr.push("\", \n");			// символ кавычек, запятую и символ новой строки				
						}
							UserSrcPathStringArr.push("\t\t\t");			// добавляем табуляцию
							UserSrcPathStringArr.push("\"");				// добавляем символ кавычек
							UserSrcPathStringArr.push(UserSrcPathArr![0]);		// добавляем путь
							UserFirstRead = false;							// сбрасываем флаг первого чтения
						}							
				}
				
				// если еще не был найдет ни один define  
				else
				{
					if(UserSrcPathArr![0] === "userSrcPaths")					// если нашли начало пользовательских путей
					{
						FoundedUserSrcPath = true;							// поднимаем флаг					
					}
				}
			}
			
			// проверим, есть ли хоть один пользовательских путей
			if(UserSrcPathStringArr.length !== 0)
			{
				// добавляем символ кавычек и новой строки
				UserSrcPathStringArr.push("\"\n");				
				// преобразуем массив пользовательсих Define в строку
				UserSrcString = UserSrcPathStringArr.join("");	
			}

			


		}
	}

	//#region !Выполним поиск добавленных пользователем папок в проект, которые он, например не указал руками

	


	//#endregion -----------------------------------------------------------------------------

	// получим значение для поля name, которое является названием проекта:
	let NameField: string = vscode.workspace.name!;

	// получим путь к файлу комилятора - arm-none-eabi-gcc.exe
	let ext: vscode.Uri = GetMyExtentionUri();
	let ArmGccCompilePath: string = ext.path.slice(1) + "/ArmGcc/bin/arm-none-eabi-gcc.exe";

	// получим путь к include файлам комилятора - arm-none-eabi-gcc.exe
	let ArmGccIncPath: string = ext.path.slice(1) + "/ArmGcc/lib/gcc/arm-none-eabi/9.2.1/include";

	// получим содержимое Makefile
	let Makefile: string = await GetMakeFileStringData();
	if(Makefile === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	// получим список define, include из make файла
	// для define:
	let DefArr;
	let DefStringArr:string[] = [];
	let DefString: string;
	// для include:
	let IncArr;
	let IncStringArr:string[] = [];
	let IncString: string;


	// 1. подготовим форму регулярного выражения для поиска define в  Make файле
	let DefRegex =  new RegExp('(?<=\-D).+\\b', 'g');
		
	// создадим переменную для отслеживания первого чтения define
	let FirstRead: boolean = true;
	// найдем все значения define
	while(DefArr = DefRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			DefStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		DefStringArr.push("\t\t\t\t");			// добавляем табуляцию
		DefStringArr.push("\"");				// добавляем символ кавычек
		DefStringArr.push(DefArr![0]);			// добавляем define
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	DefStringArr.push("\"\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	DefString = DefStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(DefString === null)
	{
		return false;
	}





	// 2. подготовим форму регулярного выражения для поиска include в  Make файле
	let IncRegex =  new RegExp('(?<=\-I).+\\b', 'g');
	
	// обновим значение переменной для первого чтения 
	FirstRead = true;
	// найдем все значения include
	while(IncArr = IncRegex.exec(Makefile))
	{
		if(!FirstRead)							// если это не первая запись, то надо перед следующим элементом добавить
		{
			IncStringArr.push("\", \n");		// символ кавычек, запятую и символ новой строки				
		}
		IncStringArr.push("\t\t\t\t");			// добавляем табуляцию
		IncStringArr.push("\"");				// добавляем символ кавычек
		IncStringArr.push(IncArr![0]);			// добавляем include
		FirstRead = false;						// сбрасываем флаг первого чтения
	}
	IncStringArr.push("\"\n");					// для последнего элемента добавляем ковычку и символ новой строки				
	
	// преобразуем массив Define в строку
	IncString = IncStringArr.join("");			

	// если строка оказалась пустой - вернем ошибку
	if(IncString === null)
	{
		return false;
	}


	


	// получим URI корневой папки (рабочей области) (ws -> workspace)
	let vscode_path: vscode.Uri;
	vscode_path = ws.with({ path: posix.join(ws.path, '.vscode') });


	// создаем контент для записи
	let Content: string = 
	"{\n" + 
	"\t\"version\": 4," + "\n" +
	"\t\"env\":" + "\n" +
	"\t{" + "\n" +
	"\t\t" + "\"_USER_DEF_START_\": \"\"," + "\n" +
	"\t\t" + "\"userDefines\": " + "\n" +
	"\t\t" + "[" + "\n" +
			UserDefString! +
	"\t\t" + "]," + "\n" +
	"\t\t" + "\"_USER_DEF_END_\": \"\"," + "\n" +
	"\n"+
	"\t\t" + "\"_USER_INC_PATHS_START_\": \"\"," + "\n" +
	"\t\t" + "\"userIncPaths\": " + "\n" +
	"\t\t" + "[" + "\n" +
			UserPathString! +
	"\t\t" + "]," + "\n" +
	"\t\t" + "\"_USER_INC_PATHS_END_\": \"\"," + "\n" +
	"\n"+
	"\t\t" + "\"_USER_SRC_PATHS_START_\": \"\"," + "\n" +
	"\t\t" + "\"userSrcPaths\": " + "\n" +
	"\t\t" + "[" + "\n" +
			UserSrcString! +
	"\t\t" + "]," + "\n" +
	"\t\t" + "\"_USER_SRC_PATHS_END_\": \"\"" + "\n" +
	"\t}," + "\n" +
	"\t\"configurations\":" + "\n" +
	"\t[" + "\n" +
	"\t\t{" + "\n" +
	"\t\t\t" + "\"name\": \"" + NameField + "\"," + "\n" +
	"\t\t\t" + "\"intelliSenseMode\": \"gcc-arm\"," + "\n" +
	"\t\t\t" + "\"cStandard\": \"c99\"," + "\n" +
	"\t\t\t" + "\"cppStandard\": \"c++17\"," + "\n" +
	"\t\t\t" + "\"compilerPath\": \"" + ArmGccCompilePath + "\"," + "\n" +
	"\t\t\t" + "\"defines\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"${userDefines}\"," + "\n" +
				DefString! + 
	"\t\t\t" + "]," + "\n" +
	"\t\t\t" + "\"includePath\":" + "\n" +
	"\t\t\t" + "[" + "\n" +
	"\t\t\t\t" + "\"${userIncPaths}\"," + "\n" +
	"\t\t\t\t" + "\"${workspaceFolder}\"," + "\n" +
	"\t\t\t\t" + "\"" + ArmGccIncPath + "\"," + "\n" +
				IncString! + 
	"\t\t\t" + "]" + "\n" +
	"\t\t}" + "\n" +
	"\t]" + "\n" +
	"}";

	// создаем c_cpp_properties.json файл и заполняем
	vscode.workspace.fs.writeFile(vscode_path.with({ path: posix.join(vscode_path.path, 'c_cpp_properties.json') }), Buffer.from(Content));


	return true;
}


/**
 * @description  функция для созадания settings.json файла
 * @returns {boolean} true - если файл успешно создан
 */
async function CreateLaunchJsonFile(): Promise<boolean> 
{	
	// проверяем наличие рабочей области 
	if(!vscode.workspace.name)
	{
		return false;
	}

	// получим URI корневой папки (рабочей области) (ws -> workspace) и папки .vscode
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;
	let vscode_path: vscode.Uri;
	vscode_path = ws.with({ path: posix.join(ws.path, '.vscode') });

	// получим название для файла .elf
	let ElfName: string = vscode.workspace.name!;

	// ссылка на stlink.cfg
	let ext: vscode.Uri = GetMyExtentionUri();
	let StlinkCfgAddr: string = ext.path.slice(1) + "/OpenOCD/scripts/interface/stlink.cfg";

	// путь до armgcc/bin
	let ArmGccBinPath: string = ext.path.slice(1) + "/ArmGcc/bin";

	// получим содержимое Makefile
	let Makefile: string = await GetMakeFileStringData();
	if(Makefile === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	// получим путь к SVD файлу микрконтроллера
	let svdFilePath: string = await GetMcuSvdPath(Makefile);
	if(svdFilePath === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}


	// получим путь к cfg файлу микрконтроллера
	let cfgFilePath: string = await GetMcuCfgPath(Makefile);
	if(cfgFilePath === "Nothing")
	{
		// возвращаем ошибку
		return false;
	}

	

	// создаем контент для записи
	let Content: string = 
	"{\n" + 
	"\t\"version\": \"0.2.0\"," + "\n" +
	"\t\"configurations\":" + "\n" +
	"\t[\n" +
	"\t\t{"+ "\n" +
	"\t\t\t\"name\": \"Cortex debug\"," + "\n" +
	"\t\t\t\"type\": \"cortex-debug\"," + "\n" +
	"\t\t\t\"request\": \"launch\"," + "\n" +
	"\t\t\t\"servertype\": \"openocd\"," + "\n" +
	"\t\t\t\"cwd\": \"${workspaceFolder}\"," + "\n" +
	"\t\t\t\"armToolchainPath\": \"" + ArmGccBinPath + "\"," + "\n" +
	"\t\t\t\"executable\": \"build/" + ElfName + ".elf\"," + "\n" +
	"\t\t\t\"svdFile\": \"" + svdFilePath + "\"," + "\n" +
	"\t\t\t\"configFiles\":\n" +
	"\t\t\t[\n" + 
	"\t\t\t\t\""+ StlinkCfgAddr + "\"" + "," + "\n" +
	"\t\t\t\t\"" + cfgFilePath + "\"" + "\n" +
	"\t\t\t]," + "\n" +
	"\t\t\t\"preLaunchTask\": \"Build project\"" + "\n" +
	"\t\t}" + "\n" +
	"\t]" + "\n" +
	"}";

	// создаем launch.json файл и заполняем
	vscode.workspace.fs.writeFile(vscode_path.with({ path: posix.join(vscode_path.path, 'launch.json') }), Buffer.from(Content));


	// все успешно
	return true;
}


//#region Вспомогательные функции

/**
 * @description  функция для задержки
 */
function delay(ms: number) 
{
    return new Promise( resolve => setTimeout(resolve, ms) );
}


/**
 * @description  функция для запуска файла CubeMx
 * @returns {string} - если все хорошо - вернет true
 */
async function OpenCubeMxApp(): Promise<boolean>
{
	// получим URI корневой паки
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;

	// создадим регулярное выражение для поиска файла проекта .ioc
	let IncRegex =  new RegExp('(.*)(\.ioc)', 'g');

	for (const [name, type] of await vscode.workspace.fs.readDirectory(ws)) 	// проходимся по всем файлам проекта
	{
		// проверяем, что это файл, а не папка
		if (type === vscode.FileType.File) 
		{
			// проверяем регуляркой, является ли файл, файлом проекта CubeMx
			if(IncRegex.exec(name) !== null)
			{
				// запускаем файл
				if((await vscode.env.openExternal(ws.with({ path: posix.join(ws.path, name) }))))
				{
					return true;
				}
			}
		}
	}


	return false;
}



/**
 * @description  функция для получения содержимого Makefile в формате string
 * @returns {string} - если Makefile НЕ удалось прочитать - вернет "Nothing"
 */
async function GetMakeFileStringData(): Promise<string> 
{
	// получим URI корневой паки
	let ws: vscode.Uri = vscode.workspace.workspaceFolders![0]!.uri;

	// создадим переменную
	let readDataArr;
	let readData: string = "Nothing";

	for (const [name, type] of await vscode.workspace.fs.readDirectory(ws)) 	// проходимся по всем файлам проекта
	{
		// проверяем, что это файл, а не папка
		if (type === vscode.FileType.File) 
		{
			// выделяем из имени - тип и ищем Make файл
			if(name === "Makefile")
			{
				// получаем данные Make файла и конвертируем в string
				readDataArr = await vscode.workspace.fs.readFile(ws.with({ path: posix.join(ws.path, name) }));
				readData =  new util.TextDecoder("utf-8").decode(readDataArr);
			}
		}
	}

	// возвращаем содержимое Makefile
	return readData;
	
}



/**
 * @description  функция для пути к svd файлу контроллера в формате string
 * @param {string}  содержимое MAkefile
 * @returns {string} - если НЕ удалось получить путь - вернет "Nothing"
 */
async function GetMcuSvdPath(Makefile: string): Promise<string> 
{
	// создадим переменную с путем к svd файлу контроллера 
	let svdPath: string = "Nothing";
	let PathName: string;
	let svdFilehName: string;
	
	// получим путь к папке расширения
	let ext: string = GetMyExtentionUri().path.slice(1);

	// подготовим форму регулярного выражения для поиска модели контроллера для подбора svd файла
	let SvdRegex =  new RegExp('(?<=\-DSTM32).+\\b');
	let RegexString: string;
	let RegexArr = SvdRegex.exec(Makefile);
	let McuName;
	if(RegexArr![0])
	{
		// строка, полученная регулярным выражением
		RegexString = RegexArr![0];

		// получим имя папки
		PathName = "STM32" + RegexString.substring(0,2);

		// получим имя файла .svd
		switch(RegexString.substring(0,2))
		{
			// если серия FO
			case ("F0"):
				svdFilehName = "STM32F0x" + RegexString.substring(3,4) + ".svd";
				break;

			// если серия F1
			case ("F1"):
				svdFilehName = "STM32F10" + RegexString.substring(3,4)  + ".svd";
				break;

			// если серия F2
			case ("F2"):
				svdFilehName = "STM32" + RegexString.substring(0,3) + "x.svd";
				break;

			// если серия F3
			case ("F3"):
				svdFilehName = "STM32F30x.svd";
				break;

			// если серия F4
			case ("F4"):
				// если F40x или F41x
				if((RegexString.substring(2, 3) === "0") || (RegexString.substring(2, 3) === "1"))
				{
					svdFilehName = "STM32" + RegexString.substring(0, 3) + "x" + ".svd";
				}
				else
				{
					svdFilehName = "STM32" + RegexString.substring(0, 4) + "x.svd";
				}                    
				break;
				break;

			// если серия F7
			case ("F7"):
				svdFilehName = "STM32" + RegexString.substring(0,2) + "x" + RegexString.substring(3,4) + ".svd";
				break;	
			
			// если серия L0
			case ("L0"):
				McuName = RegexString.substring(0,3);
				if(McuName === "L05")
				{
					svdFilehName = "STM32L05x.svd";
				}
				else if(McuName === "L06")	
				{
					svdFilehName = "STM32L06x.svd";
				}
				else if(McuName === "L07")	
				{
					svdFilehName = "STM32L07x.svd";
				}
				else
				{
					svdFilehName = "STM32L0x.svd";
				}			
				break;
			
			// если серия L1
			case ("L1"):
				McuName = RegexString.substring(0,3);
				if(McuName === "L15")
				{
					svdFilehName = "STM32L15x.svd";
				}
				else if(McuName === "L10")
				{
					svdFilehName = "STM32L10x.svd";
				}
				else
				{
					svdFilehName = "STM32L1x.svd";
				}
				break;

			// если серия L4
			case ("L4"):
				svdFilehName = "STM32L4" + "x" + RegexString.substring(3,4) + ".svd";
				break;
						
			// если контроллер не в списке
			default:
				return svdPath;   // вернет Nothing
		}

		// формируем путь к svd файлу контроллера
		svdPath = ext + "/SvdTools/" + PathName + "/" + svdFilehName;
	}

	// вернем путь к svd файлу контроллера 
	return svdPath;
}


/**
 * @description  функция для пути к cfg файлу контроллера в формате string
 * @param {string}  содержимое MAkefile
 * @returns {string} - если НЕ удалось получить путь - вернет "Nothing"
 */
async function GetMcuCfgPath(Makefile: string): Promise<string>
{
	// создадим переменную с путем к svd файлу контроллера 
	let cfgPath: string = "Nothing";
	let cfgFilehName: string;
		
	// получим путь к папке расширения
	let ext: string = GetMyExtentionUri().path.slice(1);

	// подготовим форму регулярного выражения для поиска модели контроллера для подбора svd файла
	let SvdRegex =  new RegExp('(?<=\-DSTM32).+\\b');
	let RegexString: string;
	let RegexArr = SvdRegex.exec(Makefile);
	if(RegexArr![0])
	{
		// строка, полученная регулярным выражением
		RegexString = RegexArr![0];

		// определим серию
		if(RegexString.substring(0,1) === "F")
		{
			// получим имя файла
			cfgFilehName = "stm32f" + RegexString.substring(1,2) + "x.cfg";
		} 
		else if(RegexString.substring(0,1) === "L")
		{
			// если 4 серия
			if(RegexString.substring(1,2) === "4")
			{
				// получим имя файла
				cfgFilehName = "stm32l4x.cfg";
			}
			else if(RegexString.substring(1,2) === "0")
			{
				// получим имя файла
				cfgFilehName = "stm32l0.cfg";
			}
			else if(RegexString.substring(1,2) === "1")
			{
				// получим имя файла
				cfgFilehName = "stm32l1.cfg";
			}
			else
			{
				// значит не поддерживаемая серия - возвращаем ошибку
				return cfgPath;
			}
		}
		else
		{
			// значит не поддерживаемая серия - возвращаем ошибку
			return cfgPath;
		}

		// формируем адрес папки
		cfgPath = ext + "/OpenOCD/scripts/target/" + cfgFilehName;
		
	}

	return cfgPath;
}

/**
 * @description  функция возвращает URI расширения STM-Helper
 * @returns {vscode.Uri} vscode.Uri - location of extentention
 */
function GetMyExtentionUri(): vscode.Uri 
{
	// находим наше расширение по ID и возвращаем его Uri
	return vscode.Uri.file(vscode.extensions.all[vscode.extensions.all?.findIndex(element => element.id === "BuzzyElectronics.stm-helper")].extensionPath);
}

//#endregion