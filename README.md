# STM-Helper README

This extension helps to automatically build the project for work in VScode with one click. 

Allows you to `Build` the firmware and `Download` it to the microcontroller. 

It also allows you to `open CubeMx project file` from the VsCode application.

<img src=https://bitbucket.org/nsb7656/stm-helper/raw/a679a8c928162fad93778fad9204922ea6779bf7/ReadMe.gif width=232 height=384>



# Features:
This extension supports the following list of controllers:
- STM32F-series (STM32F0-F4 & STM32F7 series)
- STM32L-series (STM32L0,L1,L4 series)

-------
You may use your user paths with your `".c"` and `".h"` files and `global includes` in project (see "Presets" paragraph 7)

-------

Supports ST-Link programmer for download firmware.

(Tests only with x64 Windows systems)

# Presets:
To work with this extension you will need the following programs:
- [STM32CubeMX](https://www.st.com/en/development-tools/stm32cubemx.html/ "Download")
- [ST-Link driver](https://www.st.com/en/development-tools/stsw-link009.html "Download")

If you want to use real-time Debug too - you should install this extension:
- [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug/ "Install")

# How to use:
1. You should build the project as `Makefile` in STM32CubeMX, with the following parameters:
 
- >System Core -> SYS -> `Debug Serial Wire`
 
- >Project Manager -> Code Generator -> `Generate peripheral initialization as a pair of '.c/.h' files per peripheral`

2. Open the project folder in VS Code and you can see the three buttons in your the lower left corner of the screen:

3. Push `"UpdateWorkspace"` button and your project will be ready! - Only one click!

<img src=https://bitbucket.org/nsb7656/stm-helper/raw/c4fc4fa0f842a440f057c14d6c418eb18445a4ae/UpdateWS.jpg width=354 height=21>


4. If you want to `Build` and `Download` your firmware - for it you have remainig two buttons.

5. If you want to start `Real-time Debug` - press `F5` button on your PC.

6. If you need to quickly open your `CubeMx project`, you may do this from VsCode by `OpenCubeMxProject` button.

<img src=https://bitbucket.org/nsb7656/stm-helper/raw/c4fc4fa0f842a440f057c14d6c418eb18445a4ae/OpenCubeMxProject.JPG width=360 height=20>

7. If you want to use your own paths with your `.c/.h` files and `global includes` in your project -> you should write `this paths` and `includes` in `.vscode/c_cpp_properties.json` file:

<img src=https://bitbucket.org/nsb7656/stm-helper/raw/072562ea45bb610b1e00836ccd60aafeb82f351e/UserFiles.JPG width=518 height=552>


# And video - How to use:
https://www.youtube.com/watch?v=N0W-mCnIRws

# Mail for issues and questions:
buzzyelectronics@gmail.com

# Our Instagram:
https://www.instagram.com/buzzy_electronics/



